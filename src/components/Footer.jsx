import React from "react";
import '../App.css';

export default function Footer() {
  return (
    <div
      className="footer d__flex align__items__center justify__content__space__between"
      style={{ padding: "1px 80px", zIndex: "100", backgroundColor:"#2c2c2c"}}
    >
      <span
        className="copyright"
        style={{ color: "#c6c9d8", fontSize: "0.8rem", opacity: "0.75", margin:"auto"}}
      >
         © 2022 Mohamed Lakzir.
      </span>
    </div>
  );
}

