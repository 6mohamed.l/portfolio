import React from "react";
import styles from './Accueil.module.css';
import CV_Mohamed from '../resources/img/CV_Mohamed Lakzir.pdf';
import Photo from '../resources/img/Profile.png';


export default function Accueil() {
    //Page d accueil de profil style minimaliste
    return (
        <div className={styles.accueil} id="Accueil">
                <div className={styles.container}>

                <div className="row">
                        
                        <div className='col__2'>

                    <div className={styles.accueil__content}>
                        <div>
                            <h2 className={styles.accueil__text}>Hello, je suis Mohamed Lakzir</h2>

                            <div className={styles.profAnim}>
                            <div className={styles.profAnim_move}>
                            <div className={styles.profAnims}>Développeur Web</div>
                            <div className={styles.profAnims}>Designer UI</div>
                            <div className={styles.profAnims}>Graphiste</div>
                            <div className={styles.profAnims}>Photographe</div>

                            </div>
                            </div>

                            <h4 className={styles.accueil__text}>J'aime le code et le design !</h4>
                            <p className={styles.apropos__text}>
                            Mon but : aider un maximum de personnes à développer leurs projets grâce à des plateformes fluides et innovantes..
                            Que ce soit pour un projet d'application web, de site personnel ou encore un portfolio. </p>
                                
                                {/*Boutton de telechargement CV*/}
                                <div>
                                    <a href={CV_Mohamed} target="_blank" rel="noopener noreferrer" download>
                                        <button className="accueil btn pointer">Télécharger CV</button></a>
                                </div>
                        </div>
                        </div>
                        </div>
                        <div className='col__2'>
                            <img src={Photo}  alt="" className={styles.profile__img} />
                        </div>

                    </div>
                </div>
        </div>
    )
}

