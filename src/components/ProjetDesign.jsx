import React from "react";
import styles from './Projet.module.css';
import designImg from '../resources/img/Design_Pub.jpg'; 
import designIcon from '../resources/img/DesignIcons.png'; 

export default function ProjetWeb(){
    return(
        <div className={styles.projetDesign} id="ProjetDesign">
                <div className='container'>
                    <div className="row"> {/*Diviser le contenu de la page en 2 colones*/}
                        <div className='col__2'>
                            <img src={designImg}  alt="" className={styles.web__img} />
                        </div>
                        <div className='col__2'>
                            <h4>Campagne publicitaire</h4>
                            <h2>COP22</h2>
                            <div>
                                <p className="web__text p__color">
                                Creation et adaptation de tous supports publicitaire "Logo, Affiches, Design de surfaces...", pour la conférence
                                mondiale au sujet de changements climatiques...
                                </p>
                                <h3>Construit avec :</h3>
                                <img src={designIcon}  alt="" className={styles.Design__img} />

                                {/* Un bouton pour diriger vers un autre site*/}
                                <div className="web__button d__flex align__items__center">
                                    <a href="https://mediamarketing.ma/article/EHZHDBA/la_cop22__lance_ae_le_temps_de_l___action_au_un_spot_qui_interpelle_sur_le_changement_climatique.html?id=0" target="_blank" rel="noreferrer"><button className="web btn pointer">Voir le projet</button></a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </div>
            
    );
}