import styles from './Header.module.css';
import Logo from '../resources/img/Logo_ML.jpg';
import MenuNav from './MenuNav';


export default function Header(props) {
        return<header className={styles.header}>
          <div className={styles.header__bg}>
              <div className="header d__flex align__items__center ">
                <div className="logo">
                  <img src={Logo} alt="" />
                </div>
              </div>
          </div>

          <MenuNav changePage={props.changePage} />

        </header>
      }