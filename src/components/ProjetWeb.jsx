import React from "react";
import styles from './Projet.module.css';
import webImg from '../resources/img/Oplaisir-site-web.jpg';
import techImg from '../resources/img/tech.png';


export default function ProjetWeb(){

    return(
        <div className={styles.projetWeb} id={styles.ProjetWeb}>
                <div className='container'>
                    <div className="row">
                        <div className="col__2">
                            <img src={webImg}  alt="" className={styles.web__img}/>
                        </div>
                        <div className="col__2">
                            <h4>Site web</h4>
                            <h2>O'Plaisir Restaurant</h2>
                            <div>
                                <p className='web__text p__color'>
                                L'objectif du projet était de la realisation d'un site web pour le restaurant O'Plaisir.
                                Un site web attractif responsif qui donne à l'utilisateur la possibilité de s'inscrire rapidement avec son pseudo
                                et de faire des commandes...
                                </p>
                                        <h3>Construit avec :</h3>
                                          <img src={techImg}  alt="" className={styles.tech__img}/>

                                    
                                <div className='projet btn d__flex align__items__center'>
                                    <a href="https://gitlab.com/servwebprojetresto/oplaisirrestaurant" target="_blank" rel="noreferrer"><button className="btn pointer">Voir le projet</button></a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </div>
            
    );
}