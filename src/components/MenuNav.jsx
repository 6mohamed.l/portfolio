import React, {useState} from "react";
import './MenuNav.css';
import '../App.css'; //Importation aussi de App.css qui contient des styles globales


export default function MenuNav(props) {

  const [visible, setVisible] = useState(false);

    return (
      <div className="header d__flex align__items__center pxy__30">

        {/* Menu principal*/}
        
            <div className="navigation pxy__30">

              <ul className="navbar d__flex">
                <li className='nav__items mx__15'>
                  <button onClick={props.changePage('accueil')}>Accueil</button>
                </li>
                <li className='nav__items mx__15'>
                  <button onClick={props.changePage('projetWeb')}>Projet WEB</button>
                </li>
                <li className='nav__items mx__15'>
                  <button onClick={props.changePage('projetDesign')}>Projet Design</button>
                </li>
                <li className='nav__items mx__15'>
                  <button onClick={props.changePage('contact')}>Contact</button>
                </li>                
              </ul>
            </div>
            

            {/* Side Menu*/}

            <div className="toggleContent">
            {/* Importation d'une icone de sideMenu*/}
            <svg onClick={() => setVisible(!visible)}
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  class="bi bi-justify pointer"
                  viewBox="0 0 16 16"
                >
                  <path
            fill-rule="evenodd"
            d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"
          />
                </svg>
          </div>
          
      {visible ? (
        <div className="sideNavbar">
        <ul className="sidebar d__flex">
        <li className="sideNavbar">
          <button onClick={props.changePage('accueil')}>Accueil</button>
        </li>
        <li className="sideNavbar">
          <button onClick={props.changePage('projetWeb')}>Projet WEB</button>
        </li>
        <li className="sideNavbar">
          <button onClick={props.changePage('projetDesign')}>Projet Design</button>
        </li>
        <li className="sideNavbar">
          <button onClick={props.changePage('contact')}>Contact</button>
        </li>    
        </ul>
     </div>
      ): null}
      </div>
    );

}
