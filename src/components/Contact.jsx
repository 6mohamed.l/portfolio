import { useState } from "react";
import './Contact.css';
import contactImg from "../resources/img/Contact.png"


export default function Contact() {
  // Déclaration d'une nouvelle variable d'état « message »
  const [message, setMessage] = useState(false);
    // creation de variable "msgEnvoyer" apres l'envoi de message (Pas encore de validation )
  const msgEnvoyer = (e) => {
    e.preventDefault();
    setMessage(true);
  };
  // retourner un formulaire
    return (
        <div className="contact component__space" id="Contact">
            <div className="row">
                <div className="col__2">
                    <div className="contact__box">
                        <div className="contact__meta">
                            <h1 className="contact__text">Contactez-moi</h1>
                            <p className="contact__text white">Envie de me présenter un projet, ou tout simplement dire bonjour ?</p>
                        </div>
                        <div className="input__box">
                        <form onSubmit={msgEnvoyer}>
                           <input type="text" className="contact nom" placeholder="Nom *" />
                           <input type="text" className="contact email" placeholder="Email *" />
                           <input type="text" className="contact sujet" placeholder="Sujet" />
                           <textarea name="message" id="message" placeholder="Message"></textarea>
                           <button className="btn contact pointer" type="submit">Envoyer</button>
                           {message && <span>Merci, Votre message a bien été envoyé !</span>}
                        </form>
                        </div>
                    </div>
                </div>
                <div className="col__2">
                    <img src={contactImg} alt="" className="contact__img" />
                </div>
            </div>
        </div>
    )
}


