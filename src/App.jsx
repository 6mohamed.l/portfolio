import styles from './App.css';
import { useState } from 'react';
import 'normalize.css';

import Header from './components/Header';
import Accueil from './components/Accueil';
import ProjetWeb from './components/ProjetWeb';
import ProjetDesign from './components/ProjetDesign';
import Contact from './components/Contact';
import Footer from './components/Footer';

export default function App() {
    // Déclaration d'une nouvelle variable d'état « pageCourante » pour changer entre les pages du menu
  const [pageCourante, setPageCourante] = useState('accueil');
  const changePage = (page)=>{
    return()=>{
      setPageCourante(page);
    }
  }
// Navigation entre les pages
  return (
    <div className={styles.App}>
    <>
      <Header changePage={changePage} />

      {pageCourante === 'accueil' &&
      <Accueil />
      }
      {pageCourante === 'contact' &&
      <Contact />
      }
      {pageCourante === 'projetWeb' &&
      <ProjetWeb />
      }
      {pageCourante === 'projetDesign' &&
      <ProjetDesign />
      }

      <Footer />
    </>
    </div>
  );
}